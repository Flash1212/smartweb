﻿
$employeesCSV_Fin = "$PSScriptRoot\Employees_Fin.csv" 
$unknownCSV_Fin = "$PSScriptRoot\Unknowns_Fin.csv"

#Repository
$destServer = "server" #xxxx.xxxx.xxxx.xxxx or name

# Function to transfer file
function Transfer($employees, $unknown, $srv)
    {
        $login = "<username>@servername:test/"
        $pwd = "..."

        Start-Process 'C:\Program Files (x86)\PuTTY\pscp.exe' -ArgumentList ("-v -scp -pw $pwd $employees $login") -NoNewWindow -PassThru -Wait -RedirectStandardOutput "$PSScriptRoot\Employees_Transfer_Out.txt" -RedirectStandardError "$PSScriptRoot\Employees_Transfer_Err.txt"
        Start-Process 'C:\Program Files (x86)\PuTTY\pscp.exe' -ArgumentList ("-v -scp -pw $pwd $unknown $login") -NoNewWindow -PassThru -Wait -RedirectStandardOutput "$PSScriptRoot\Unknown_Transfer_Out.txt" -RedirectStandardError "$PSScriptRoot\Unknown_Transfer_Err.txt"
    }


Transfer $employeesCSV_Fin $unknownCSV_Fin $destServer