#Created by: Dominique J. Thornton

###############################SmartWeb User Extraction######################################
# This script is designed to extract TXAUS AD user accounts from Active Directory from both #
# the User/Managed Group & UserAccounts OUs in TXAUS. The script will determine whether or  #
# not the user accounts have EmployeeIDs and if they're active accounts. Active accounts w/ #
# EmployeeIDs are placed into the script directory as .\Employees.csv while Active accounts #
# w/o Employee IDs are placed into the script directory as .\Unknowns.csv.                  #
###############################SmartWeb User Extraction######################################


###############################Functions#####################################

# Main function which calls all following scripts. 
function main
    {
        #Set Variables
        #$path = "G:\IS-voip\djthornton\Scripts\PowerShell\SmartWeb\SmartWeb User Extraction.ps1"

        #DATE
        $LogDate = get-date -f yyyyMMdd

        #WRITE FILES
        $employeesCSV_Orig = "$PSScriptRoot\Employees_Orig.csv" 
        $unknownCSV_Orig = "$PSScriptRoot\Unknowns_Orig.csv"
        $employeesCSV_Fin = "$PSScriptRoot\Employees_Fin.csv" 
        $unknownCSV_Fin = "$PSScriptRoot\Unknowns_Fin.csv"
        $LogFile = "$PSScriptRoot\Logfile.txt" #Add-content - are writing to this logfile.
        $csvEF1 = "$PSScriptRoot\Managed_grp_Orig----$LogDate.csv"
        $csvEF2 = "$PSScriptRoot\UserAccounts_grp_Orig----$LogDate.csv"
        $sSetonDID = '512324'
        $sSetonMain = '5123241000', '5123247000', '5123240000', '5123245000','5123244000','5123246000','5123249000'
        $sSetonExp = '5123249999'

        #Repository
        $destServer = "SPOKINTERFACES:AD_DATAFEED/" #

        #Remove old files
        Remove-Item -Path $employeesCSV_Orig -erroraction 'silentlycontinue'
        Remove-Item -Path $employeesCSV_Fin -erroraction 'silentlycontinue'
        Remove-Item -Path $unknownCSV_Orig -erroraction 'silentlycontinue'
        Remove-Item -Path $unknownCSV_Fin -erroraction 'silentlycontinue'
        Remove-Item -Path $csvEF1 -erroraction 'silentlycontinue'
        Remove-Item -Path $csvEF2 -erroraction 'silentlycontinue'

        #Start Logging
        New-Item -Path $LogFile -ItemType File -erroraction 'silentlycontinue' -Value "$LogTime :| Beginning LogFile.`r`n"
        Log "***$LogDate***"
        Log "Get AD User info"

        AD-Get
        Search1
        Search2
        PhoneNumber1
        PhoneNumber2
        Clean-UP

        #End Script
        Log "Extraction complete."
        Log "CSV manipulation done!"

        #Transfer $employeesCSV_Fin $unknownCSV_Fin $destServer

         #Remove old files
        Remove-Item -Path $employeesCSV_Orig -erroraction 'silentlycontinue'
        Remove-Item -Path $employeesCSV_Fin -erroraction 'silentlycontinue'
        Remove-Item -Path $unknownCSV_Orig -erroraction 'silentlycontinue'
        Remove-Item -Path $unknownCSV_Fin -erroraction 'silentlycontinue'
        Remove-Item -Path $csvEF1 -erroraction 'silentlycontinue'
        Remove-Item -Path $csvEF2 -erroraction 'silentlycontinue'

    }

# Function to Get-AD accounts from 2 OUs
function AD-Get 
    {
        Log 'Import AD module'
        import-module activedirectory

        Log "Get AD User info"
        #Get-ADUser -identity dsoto025 -Properties employeeNumber, employeeID, DisplayName, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone | select employeeNumber, employeeID, DisplayName, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone | Export-Csv -Path $csvfile -NoType
        Log "Getting Managed"
        Get-ADUser -filter * -SearchBase "OU=Users,OU=Managed,DC=txaus,DC=DS,DC=SJHS,DC=com" -Properties employeeNumber, employeeID, GivenName, Surname, Initials, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone, physicalDeliveryOfficeName, streetAddress, enabled | select employeeNumber, employeeID, GivenName, Surname, Initials, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone, physicalDeliveryOfficeName, streetAddress, enabled | Export-Csv -Path $csvEF1 -NoType
        Log "Managed retrieved"

        Log 'Getting User Accounts'
        Get-ADUser -filter * -SearchBase "OU=Users,OU=User Accounts,OU=SETON,DC=txaus,DC=DS,DC=SJHS,DC=com" -Properties employeeNumber, employeeID, GivenName, Surname, Initials, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone, physicalDeliveryOfficeName, streetAddress, enabled | select employeeNumber, employeeID, GivenName, Surname, Initials, EmailAddress, Title, sAMAccountName, Department, telephoneNumber, MobilePhone, physicalDeliveryOfficeName, streetAddress, enabled | Export-Csv -Path $csvEF2 -NoType
        Log "User Accounts Retrieved"

        Log "AD Import Complete!"
        "AD Import Complete!"
    }

# Function to loop through the employee number column. If the number exist write to entry to the 
# employee file. If not then add to an unknown file for later review. (Managed)
function Search1 
    {
        #Import
        Log "Importing Managed_grp."
        $csvIF1 = Import-CSV $csvEF1

        #File count.
        Log "User count is: $($csvIF1.count)."
        $eIndex = 0
        $uIndex = 0
        $int = 0

        Log "Searching for employeeNumbers."

        $csvIF1 | ForEach-Object {
               If ($_.employeeNumber -and $_.enabled -eq $TRUE){
                    $_ | export-csv -append -path $employeesCSV_Orig -notype
                    $eIndex++
                    }
               Elseif (-Not $_.employeeNumber -and $_.enabled -eq $TRUE) {
                    $_ | export-csv -append -path $unknownCSV_Orig -notype
                    $uIndex++
                    }
            }
    "Search1 Complete"
    }


# Function to loop through the employee number column. If the number exist write to entry to the 
# employee file. If not then add to an unknown file for later review. (UserAccounts)
function Search2 
    {
        #Import
        Log "Importing UserAccounts_grp."
        $csvIF2 = Import-CSV $csvEF2

        #File count.
        Log "User count is: $($csvIF2.count)"
        $eIndex = 0
        $uIndex = 0
        $int = 0

        Log "Searching for employeeNumbers."

        $csvIF2 | ForEach-Object {
               If ($_.employeeNumber -and $_.enabled -eq $TRUE){
                    $_ | export-csv -append -path $employeesCSV_Orig -notype
                    $eIndex++
                    }
               Elseif (-Not $_.employeeNumber -and $_.enabled -eq $TRUE) {
                    $_ | export-csv -append -path $unknownCSV_Orig -notype
                    $uIndex++
                    }
            }
    "Search2 Complete"            
    }

# Function to cleanup phone numbers
function PhoneNumber1
    {
    #Import
        Log "Phone Number Mod."
        Log "Importing Managed_grp_Orig."
        $csvIF1 = Import-CSV $employeesCSV_Orig

        #File count.
        Log "User count is: $($csvIF1.count)."
        $eIndex = 0
        $uIndex = 0
        $int = 0

        Log "Searching employeeNumbers Phone Numbers."

        $csvIF1 | ForEach-Object {

                if ($_.telephoneNumber -match "x") {
                        $_.telephoneNumber = $_.telephoneNumber -replace "(.*)x(.*\s)"
                        $_.telephoneNumber = $_.telephoneNumber -replace "(.*)x"
                    }

                $_.telephoneNumber = $_.telephoneNumber -creplace "[^\d]"

                if($_.telephoneNumber.Contains($sSetonExp)){
                        $_.telephoneNumber = $_.telephoneNumber.Replace($sSetonExp, "")
                    }
           

                    
                Foreach ($number in $sSetonMain) {
                    if($_.telephoneNumber.Contains($number)) {
                        $_.telephoneNumber = $_.telephoneNumber -replace $number

                    }
                }

                if($_.telephoneNumber.Contains($sSetonDID)) {
                        $_.telephoneNumber = '4'+$_.telephoneNumber -replace $sSetonDID
                           
                    }

                $_.telephoneNumber = $_.telephoneNumber -replace '\s',''
                if($_.telephoneNumber.StartsWith('1') -and  $_.telephoneNumber.Length -gt 10) {
                        $_.telephoneNumber = $_.telephoneNumber.TrimStart('1 ')
                           
                    }
                
                $measureObject = $_.telephoneNumber | Measure -Character
                $count = $measureObject.Characters
                #$count

                if($count -eq 15) {
                        [Long]$b = $_.telephoneNumber
                        $c = “{0:### ### #### #####}” -f $b
                        $_.telephoneNumber = $c
                    }Elseif(($count -gt 5) -and ($count -le 11)) {
                        [Long]$b = $_.telephoneNumber
                        $c = “{0:### ### ####}” -f $b
                        $_.telephoneNumber = $c
                    }Elseif($count -eq 20) {
                        $splitTemp1 = $_.telephoneNumber.Substring(0,10)
                        $splitTemp2 = $_.telephoneNumber.Substring(10)
                        [Long]$b = $splitTemp1
                        [Long]$b2 = $splitTemp2
                        $c = “{0:### ### ####}” -f $b
                        $d = “{0:### ### ####}” -f $b2
                        $_.telephoneNumber = $c+","+$d

                    }
                 
                if($_.telephoneNumber -eq '' ) {
                        $_.telephoneNumber = 'No number'
                           
                    }

                $measureObject = $Null
                $count = $Null
                $b = $Null
                $c = $Null  

                if ($_.MobilePhone -match "x") {
                        $_.MobilePhone = $_.MobilePhone -replace "(.*)x(.*\s)"
                        $_.MobilePhone = $_.MobilePhone -replace "(.*)x"
                    }

                $_.MobilePhone = $_.MobilePhone -creplace "[^\d]"


                 if($_.MobilePhone.Contains($sSetonExp)){
                        $_.MobilePhone = $_.MobilePhone.Replace($sSetonExp, "")
                    }


                Foreach ($number in $sSetonMain) {
                    if($_.MobilePhone.Contains($number)) {
                        $_.MobilePhone = $_.MobilePhone -replace $number

                    }
                }

                 if($_.MobilePhone.Contains($sSetonDID)) {
                        $_.MobilePhone = '4'+$_.MobilePhone -replace $sSetonDID
                           
                    }
#>
                $_.MobilePhone = $_.MobilePhone -replace '\s',''
                if($_.MobilePhone.StartsWith('1') -and  $_.MobilePhone.Length -gt 10) {
                        $_.MobilePhone = $_.MobilePhone.TrimStart('1 ')
                           
                    }

                $measureObject = $_.MobilePhone | Measure -Character
                $count = $measureObject.Characters
                #$count
                if($count -eq 15) {
                        [Long]$b = $_.MobilePhone
                        $c = “{0:### ### #### #####}” -f $b
                        $_.MobilePhone = $c
                    }Elseif(($count -gt 5) -and ($count -le 11)) {
                        [Long]$b = $_.MobilePhone
                        $c = “{0:### ### ####}” -f $b
                        $_.MobilePhone = $c

                    }Elseif($count -eq 20) {
                        $splitTemp1 = $_.MobilePhone.Substring(0,10)
                        $splitTemp2 = $_.MobilePhone.Substring(10)
                        [Long]$b = $splitTemp1
                        [Long]$b2 = $splitTemp2
                        $c = “{0:### ### ####}” -f $b
                        $d = “{0:### ### ####}” -f $b2
                        $_.MobilePhone = $c+","+$d
                    }

                if($_.MobilePhone -eq '' ) {
                        $_.MobilePhone = 'No number'
                           
                    }

                $_ | export-csv -append -path $employeesCSV_Fin -notype  
                
                $measureObject = $Null
                $count = $Null
                $b = $Null
                $c = $Null     
            }
    "PhoneNumber1 Complete"
    }

# Function to cleanup phone numbers
function PhoneNumber2
    {
    #Import
        Log "Phone Number Mod."
        Log "Importing Unknown_grp_Orig."
        $csvIF2 = Import-CSV $unknownCSV_Orig

        #File count.
        Log "User count is: $($csvIF2.count)."
        $eIndex = 0
        $uIndex = 0
        $int = 0

        Log "Searching employeeNumbers Phone Numbers."

        $csvIF2 | ForEach-Object {

                if ($_.telephoneNumber -match "x") {
                        $_.telephoneNumber = $_.telephoneNumber -replace "(.*)x(.*\s)"
                        $_.telephoneNumber = $_.telephoneNumber -replace "(.*)x"
                    }
 
                $_.telephoneNumber = $_.telephoneNumber -creplace "[^\d]"
 
                if($_.telephoneNumber.Contains($sSetonExp)){
                        $_.telephoneNumber = $_.telephoneNumber.Replace($sSetonExp, "")
                    }
              
                Foreach ($number in $sSetonMain) {
                    if($_.telephoneNumber.Contains($number)) {
                        $_.telephoneNumber = $_.telephoneNumber -replace $number

                    }
                }

                if($_.telephoneNumber.Contains($sSetonDID)) {
                        $_.telephoneNumber = '4'+$_.telephoneNumber -replace $sSetonDID
                           
                    }

                $_.telephoneNumber = $_.telephoneNumber -replace '\s',''
                if($_.telephoneNumber.StartsWith('1') -and  $_.telephoneNumber.Length -gt 12) {
                        $_.telephoneNumber = $_.telephoneNumber.TrimStart('1 ')
                           
                    }

                $measureObject = $_.telephoneNumber | Measure -Character
                $count = $measureObject.Characters
                #$count
                if($count -eq 15) {
                    [Long]$b = $_.telephoneNumber
                    $c = “{0:### ### #### #####}” -f $b
                    $_.telephoneNumber = $c
                }Elseif(($count -gt 5) -and ($count -le 11)) {
                    [Long]$b = $_.telephoneNumber
                    $c = “{0:### ### ####}” -f $b
                    $_.telephoneNumber = $c
                }Elseif($count -eq 20) {
                    $splitTemp1 = $_.telephoneNumber.Substring(0,10)
                    $splitTemp2 = $_.telephoneNumber.Substring(10)
                    [Long]$b = $splitTemp1
                    [Long]$b2 = $splitTemp2
                    $c = “{0:### ### ####}” -f $b
                    $d = “{0:### ### ####}” -f $b2
                    $_.telephoneNumber = $c+","+$d

                }
                 
                if($_.telephoneNumber -eq '' ) {
                        $_.telephoneNumber = 'No number'
                           
                    }

                $measureObject = $Null
                $count = $Null
                $b = $Null
                $c = $Null  

                if ($_.MobilePhone -match "x") {
                        $_.MobilePhone = $_.MobilePhone -replace "(.*)x(.*\s)"
                        $_.MobilePhone = $_.MobilePhone -replace "(.*)x"
                    }

                $_.MobilePhone = $_.MobilePhone -creplace "[^\d]"

                if($_.MobilePhone.Contains($sSetonExp)){
                        $_.MobilePhone = $_.MobilePhone.Replace($sSetonExp, "")
                    }

                 Foreach ($number in $sSetonMain) {
                    if($_.MobilePhone.Contains($number)) {
                        $_.MobilePhone = $_.MobilePhone -replace $number

                    }
                }

                 if($_.MobilePhone.Contains($sSetonDID)) {
                        $_.MobilePhone = '4'+$_.MobilePhone -replace $sSetonDID
                           
                    }

                $_.MobilePhone = $_.MobilePhone -replace '\s',''
                if($_.MobilePhone.StartsWith('1') -and  $_.MobilePhone.Length -gt 12) {
                        $_.MobilePhone = $_.MobilePhone.TrimStart('1 ')
                           
                    }

                $measureObject = $_.MobilePhone | Measure -Character
                $count = $measureObject.Characters
                #$count
                if($count -eq 15) {
                    [Long]$b = $_.MobilePhone
                    $c = “{0:### ### #### #####}” -f $b
                    $_.MobilePhone = $c
                }Elseif(($count -gt 5) -and ($count -le 11)) {
                    [Long]$b = $_.MobilePhone
                    $c = “{0:### ### ####}” -f $b
                    $_.MobilePhone = $c
                }Elseif($count -eq 20) {
                    $splitTemp1 = $_.MobilePhone.Substring(0,10)
                    $splitTemp2 = $_.MobilePhone.Substring(10)
                    [Long]$b = $splitTemp1
                    [Long]$b2 = $splitTemp2
                    $c = “{0:### ### ####}” -f $b
                    $d = “{0:### ### ####}” -f $b2
                    $_.MobilePhone = $c+","+$d

                }

                if($_.MobilePhone -eq '' ) {
                        $_.MobilePhone = 'No number'
                           
                    }


                $_ | export-csv -append -path $unknownCSV_Fin -notype     
                
                $measureObject = $Null
                $count = $Null
                $b = $Null
                $c = $Null    
            }
    "PhoneNumber2 Complete"
    }

# Function to cleanup Variables
function Clean-UP
    {
        Log "Cleanup employeeCSV variable."
        Remove-Variable $employeesCSV_Orig -erroraction 'silentlycontinue'
        Remove-Variable $employeesCSV_Fin -erroraction 'silentlycontinue'
        Log "Cleanup unknownCSV_Orig variable."
        Remove-Variable $unknownCSV_Orig -erroraction 'silentlycontinue'
        Remove-Variable $unknownCSV_Fin -erroraction 'silentlycontinue'
    "Cleanup Complete"
    }

# Function designed for logging
function Log($entry)
    {
        
        #Time
        $LogTime = Get-Date -f HH:mm:ss
        Add-content -Path $Logfile -value "$LogTime :| $entry"
    }

# Function to transfer file
# Function to transfer file
function Transfer($employees, $unknown, $srv)
    {
        $login = "amcom@$srv"
        $pwd = "########"

        Start-Process "$PSScriptRoot\pscp.exe" -ArgumentList ("-i $PSScriptRoot\smartwebKey.ppk -v -scp $employees $login") -NoNewWindow -PassThru -Wait -RedirectStandardOutput "$PSScriptRoot\Employees_Transfer_Out.txt" -RedirectStandardError "$PSScriptRoot\Employees_Transfer_Err.txt"
        Start-Process "$PSScriptRoot\pscp.exe" -ArgumentList ("-i $PSScriptRoot\smartwebKey.ppk -v -scp $unknown $login") -NoNewWindow -PassThru -Wait -RedirectStandardOutput "$PSScriptRoot\Unknown_Transfer_Out.txt" -RedirectStandardError "$PSScriptRoot\Unknown_Transfer_Err.txt"
    }

#Begin Script
main

#Testing
$1st = "$PSScriptRoot\Test.txt"
$2nd = "$PSScriptRoot\Test.txt"
$srv = 'SpokInterfaces:AD_DATAFEED/'
Transfer $1st $2nd $srv

